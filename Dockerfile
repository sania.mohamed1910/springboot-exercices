# Étape 1: Utiliser une image Maven pour construire le jar
#FROM maven:3.8.4-openjdk-11 as build
#WORKDIR /app

#RUN ./mvnw dependency:go-offline
#ARG NODE_ENV=development
#ENV NODE_ENV=$NODE_ENV
#
## Copier les fichiers pom.xml et src dans le conteneur
#COPY pom.xml .
#COPY src ./src
## Construire l'application
#RUN ./mvnw clean package
#
## Étape 2: Utiliser une image Java pour exécuter l'application
#FROM openjdk:17-jdk-slim
#WORKDIR /app
## Copier le jar construit depuis l'étape de build
#COPY --from=build /app/target/*.jar app.jar
## Exposer le port utilisé par l'application
#EXPOSE 8080
## Définir la commande pour démarrer l'application
#CMD ["java", "-jar", "app.jar"]


#Generate build
FROM maven:3-eclipse-temurin-17-alpine as build
WORKDIR /scanner
COPY pom.xml .
COPY src ./src
RUN mvn clean package - Dmaven.test.skip=true

# Dockerizer notre application

FROM eclipse-temurin:21.0.2_13-jdk-jammy
WORKDIR /scanner
COPY --from=build /scanner/target/demo-0.0.1-SNAPSHOT.jar ./scanner.jar
EXPOSE 5000
ENTRYPOINT [ "java", "-jar", "scanner.jar" ]

