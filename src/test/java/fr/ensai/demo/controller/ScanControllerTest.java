package fr.ensai.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.FichierService;
import fr.ensai.demo.service.ScanService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ScanControllerTest {

    @InjectMocks
    private ScanController scanController;

    @Mock
    private ScanService scanService;

    @Mock
    private FichierService fichierService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createScan_ShouldCreateNewScan_WhenValidParametersProvided() {
        // Given
        String repertoire = "User/";
        int profondeurMaxArb = 3;
        int nbrMaxFichiers = 2;
        String filtreNomFichier = "requirements";
        String filtreTypeFichier = "*.txt";
        Scan mockScan = new Scan(); 
        when(scanService.createScan(repertoire, profondeurMaxArb, nbrMaxFichiers, filtreNomFichier, filtreTypeFichier)).thenReturn(mockScan);

        // When
        ResponseEntity<Scan> response = scanController.createScan(repertoire, profondeurMaxArb, nbrMaxFichiers, filtreNomFichier, filtreTypeFichier);

        // Then
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(mockScan, response.getBody());
    }

    @Test
    public void duplicateScan_ShouldDuplicateScan_WhenValidParametersProvided() {
        // Given
        Integer id = 1;
        Integer nbrMaxFichiers = 5;
        Integer profondeurMaxArb = 4;
        String filtreNomFichier = "data";
        String filtreTypeFichier = "*.csv";
        Scan duplicatedScan = new Scan(); // Initiez avec les valeurs attendues
        when(scanService.duplicateScan(id, nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier)).thenReturn(duplicatedScan);

        // When
        ResponseEntity<Scan> response = scanController.duplicateScan(id, nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);

        // Then
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(duplicatedScan, response.getBody());
    }

    @Test
    public void deleteScan_ShouldDeleteScan_WhenIdProvided() {
        // Given
        Integer id = 1;

        // When
        ResponseEntity<Void> response = scanController.deleteScan(id);

        // Then
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @SuppressWarnings("null")
    @Test
    public void getScans_ShouldReturnAllScans() {
        // Given
        when(scanService.getScans()).thenReturn(List.of(new Scan(), new Scan())); // Remplacez par des scans mockés réels

        // When
        ResponseEntity<List<Scan>> response = scanController.getScans();

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, response.getBody().size());
    }

}
