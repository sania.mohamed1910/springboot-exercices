package fr.ensai.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ensai.demo.DemoApplication;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.ScanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = DemoApplication.class)
@AutoConfigureMockMvc
public class ScanControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ScanRepository scanRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        scanRepository.deleteAll();
    }

    @Test
    public void createScan_ShouldReturnCreated_WhenValidRequest() throws Exception {
        // Given
        Scan scan = new Scan();
        scan.setRepertoire("User/");
        scan.setProfondeurMaxArb(3);
        scan.setNbrMaxFichiers(2);
        scan.setFiltreNomFichier("requirements");
        scan.setFiltreTypeFichier("*.txt");
        String scanJson = objectMapper.writeValueAsString(scan);

        // When
        mockMvc.perform(post("/scans")
                .contentType(MediaType.APPLICATION_JSON)
                .content(scanJson))

        // Then
                .andExpect(status().isCreated());
    }
}
