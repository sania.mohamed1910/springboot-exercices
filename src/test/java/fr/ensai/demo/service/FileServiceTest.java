// package fr.ensai.demo.service;

// import static org.junit.jupiter.api.Assertions.assertEquals;
// import static org.mockito.Mockito.verify;
// import static org.mockito.Mockito.when;

// import java.util.ArrayList;
// import java.util.List;

// import org.junit.jupiter.api.Test;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.springframework.boot.test.context.SpringBootTest;

// import fr.ensai.demo.model.Fichier;
// import fr.ensai.demo.repository.FichierRepository;

// @SpringBootTest
// public class FileServiceTest {

//     @Mock
//     private FichierRepository fichierRepository;

//     @InjectMocks
//     private FileService fileService;

//     @Test
//     public void testAddFileToFichier() {
//         // Given
//         Fichier fichier = new Fichier("test.txt", System.currentTimeMillis(), 500.0, "text/plain", "/test/test.txt");
//         when(fichierRepository.save(fichier)).thenReturn(fichier);

//         // When
//         Fichier result = fileService.addFileToFichier(fichier);

//         // Then
//         assertEquals(fichier, result);
//     }

//     @Test
//     public void testGetFilesByScanId() {
//         // Given
//         Integer scanId = 1;
//         List<Fichier> fichiers = new ArrayList<>();
//         fichiers.add(new Fichier("test.txt", System.currentTimeMillis(), 500.0, "text/plain", "/test/test.txt"));
//         when(fichierRepository.findByScanId(scanId)).thenReturn(fichiers);

//         // When
//         Iterable<Fichier> result = fileService.getFilesByScanId(scanId);

//         // Then
//         assertEquals(fichiers, result);
//     }

//     @Test
//     public void testDeleteFile() {
//         // Given
//         Integer fileId = 1;
//         when(fichierRepository.existsById(fileId)).thenReturn(true);

//         // When
//         fileService.deleteFile(fileId);

//         // Then
//         verify(fichierRepository).deleteById(fileId);
//     }

//     @Test
//     public void testUpdateFile() {
//         // Given
//         Fichier fichier = new Fichier("updated.txt", System.currentTimeMillis(), 600.0, "text/plain", "/test/updated.txt");
//         fichier.setId(1); // Assuming this Fichier has an ID
//         when(fichierRepository.save(fichier)).thenReturn(fichier);

//         // When
//         Fichier result = fileService.updateFile(fichier);

//         // Then
//         assertEquals(fichier, result);
//     }
// }
