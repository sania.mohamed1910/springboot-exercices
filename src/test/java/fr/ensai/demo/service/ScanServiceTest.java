package fr.ensai.demo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Optional;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.FichierRepository;
import fr.ensai.demo.repository.ScanRepository;
import fr.ensai.demo.strategy.LocalStrategy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ScanServiceTest {

    @Mock
    private ScanRepository scanRepository;

    @Mock
    private FichierRepository fichierRepository;

    @InjectMocks
    private ScanService scanService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createScan_CallsRepositorySave() {
        
        String repertoire = "OneDrive/ENSAI/1A/genie-logiciel/";
        Integer profondeurMaxArb = 5;
        Integer nbrMaxFichiers = 10;
        String filtreNomFichier = null;
        String filtreTypeFichier =  "pdf";
        scanService.createScan(repertoire, profondeurMaxArb, nbrMaxFichiers, filtreNomFichier, filtreTypeFichier);
        verify(scanRepository);
    }
        
    void getScan_WhenScanExists_ShouldReturnScan() {
        // Given
        Integer scanId = 1;
        Scan mockScan = new Scan();
        mockScan.setId(scanId);
        when(scanRepository.findById(scanId)).thenReturn(Optional.of(mockScan));

        // When
        Scan scan = scanService.getScan(scanId);

        // Then
        assertNotNull(scan);
        assertEquals(scanId, scan.getId());
    }

    @Test
    void getScan_WhenScanDoesNotExist_ShouldThrowException() {
        // Given
        Integer scanId = 99;
        when(scanRepository.findById(scanId)).thenReturn(Optional.empty());

        // When & Then
        assertThrows(RuntimeException.class, () -> {
            scanService.getScan(scanId);
        });
    }

    @Test
    void createScan_ShouldReturnNewScan() {
        // Given
        String repertoire = "User/";
        int profondeurMaxArb = 3;
        int nbrMaxFichiers = 2;
        String filtreNomFichier = "requirements";
        String filtreTypeFichier = "*.txt";
        Scan mockScan = new Scan();
        LocalStrategy localStrategy = mock(LocalStrategy.class);
        when(localStrategy.scanner(eq(repertoire), eq(profondeurMaxArb), eq(nbrMaxFichiers), eq(filtreNomFichier), eq(filtreTypeFichier))).thenReturn(mockScan);

        // When
        Scan scan = scanService.createScan(repertoire, profondeurMaxArb, nbrMaxFichiers, filtreNomFichier, filtreTypeFichier);

        // Then
        assertNotNull(scan);
    }

    @Test
    void deleteScan_WhenScanExists_ShouldDeleteScan() {
        // Given
        Integer scanId = 1;
        doNothing().when(scanRepository).deleteById(scanId);

        // When
        scanService.deleteScan(scanId);

        // Then
        verify(scanRepository).deleteById(scanId);
    }

    @Test
    void deleteScan_WhenScanDoesNotExist_ShouldThrowException() {
        // Given
        Integer scanId = 99;
        doThrow(new RuntimeException("Le scan n'est pas dans la base de données.")).when(scanRepository).deleteById(scanId);

        // When & Then
        assertThrows(RuntimeException.class, () -> {
            scanService.deleteScan(scanId);
        });
    }
    
}
