package fr.ensai.demo.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.FichierRepository;
import fr.ensai.demo.repository.ScanRepository;

@SpringBootTest 
@Transactional 
public class ScanServiceIntegration {

    @Autowired
    private ScanService scanService;

    @Autowired
    private ScanRepository scanRepository;

    @Autowired
    private FichierRepository fichierRepository;

    @BeforeEach
    void setUp() {}

    @Test
    void getScan_WhenScanExists_ShouldReturnScan() {
        // Given
    //    Scan savedScan = scanRepository.save(new Scan(...)); 

        // When
    //    Scan foundScan = scanService.getScan(savedScan.getId());

        // Then
    //    assertThat(foundScan).isNotNull();
    //    assertThat(foundScan.getId()).isEqualTo(savedScan.getId());
    }

    @Test
    void createAndDeleteScan_ShouldCreateThenDeleteScan() {
        // Given
        String repertoire = "User/";
        int profondeurMaxArb = 3;
        int nbrMaxFichiers = 2;
        String filtreNomFichier = "requirements";
        String filtreTypeFichier = "*.txt";

        // When
        Scan createdScan = scanService.createScan(repertoire, profondeurMaxArb, nbrMaxFichiers, filtreNomFichier, filtreTypeFichier);
        scanService.deleteScan(createdScan.getId());

        // Then
        assertTrue(scanRepository.findById(createdScan.getId()).isEmpty());
    }

    @Test
    void duplicateScan_WhenScanExists_ShouldDuplicateScan() {
        // Given
    //    Scan originalScan = scanRepository.save(new Scan(...)); 
    //    Integer nbrMaxFichiers = 5; 
//
    //    // When
    //    Scan duplicatedScan = scanService.duplicateScan(originalScan.getId(), nbrMaxFichiers, null, null, null);
//
    //    // Then
    //    assertThat(duplicatedScan).isNotNull();
    //    assertThat(duplicatedScan.getNbrMaxFichiers()).isEqualTo(nbrMaxFichiers);
    //    assertThat(duplicatedScan.getId()).isNotEqualTo(originalScan.getId());
    }

}
