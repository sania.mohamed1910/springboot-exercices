INSERT INTO scans (scan_id, nbr_max_fichiers, profondeur_max_arb, filtre_nom_fichier, filtre_type_fichier, date_scan, temps_exe, repertoire) 
VALUES 
(1, 2, 3, 'requirements', '*.txt', '2024-02-20 22:00:00', '5925', 'User/'),
(2, 3, 2, 'rapport', '*.pdf', '2024-02-28 10:30:00', '8098', 'User/'),
(3, 5, 5, 'donnees', '*.csv', '2024-02-27 12:45:00', '10135', 'User/');

INSERT INTO fichiers (fichier_id, scan_id, nom, date_modification, poids, type_fichier, chemin)
VALUES 
(1, 1, 'poulet_requirements.txt', '2024-02-20 09:05:00', 2048, '.txt', 'User/Sania/Projet/'),
(2, 1, 'requirements.txt', '2024-02-20 09:10:00', 3072, '.txt', 'User/Sania/'),
(3, 2, 'rapport_stage_2A.pdf', '2024-02-20 10:41:00', 4096, '.pdf', 'User/Sania/'),
(4, 2, 'projet_genie_logiciel_rapport.pdf', '2024-02-20 10:40:00', 409.6, '.pdf', 'User/'),
(5, 2, 'rapport_RL.pdf', '2024-02-20 10:43:34', 4096, '.pdf', 'User/Sania/'),
(6, 3, 'donnees.csv', '2024-02-20 21:32:59', 4096, '.csv', 'User/Sania/Projet/projet1/main/'),
(7, 3, 'donnees_parfums.csv', '2023-02-20 19:07:00', 4096, '.csv', 'User/Sania/Projet/projet1/rsc/'),
(8, 3, 'tp_donnees.csv', '2024-01-20 11:40:00', 4096, '.csv', 'User/Sania/Projet/projet1/rsc/');
