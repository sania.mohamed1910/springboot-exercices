DROP TABLE IF EXISTS scans CASCADE;

-- Création de la table des scans
CREATE TABLE scans (
    scan_id SERIAL PRIMARY KEY,
    nbr_max_fichiers INT,
    profondeur_max_arb INT,
    filtre_nom_fichier VARCHAR(255),
    filtre_type_fichier VARCHAR(50),
    date_scan TIMESTAMP,
    temps_exe FLOAT,
    repertoire VARCHAR(255)
);


DROP TABLE IF EXISTS fichiers;
-- Création de la table des fichiers
CREATE TABLE fichiers (
    fichier_id SERIAL PRIMARY KEY,
    scan_id INT REFERENCES scans(scan_id) ON DELETE CASCADE,
    nom VARCHAR(255),
    date_modification TIMESTAMP,
    poids FLOAT, -- en octets
    type_fichier VARCHAR(50),
    chemin VARCHAR(255)
);
