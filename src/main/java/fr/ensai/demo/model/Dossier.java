package fr.ensai.demo.model;

import java.util.ArrayList;
import java.util.List;

public class Dossier implements Composant { //tout objet de la classe Composant peut être considéré comme un composant dans une structure arborescente.
   
    private String name;
    private List<Composant> children; //Liste des composants d'un dossier. Ca peut être des fichiers ou des dossiers

    public Dossier(String name) { //Constructeur de la classe Dossier ; nom + liste de bébés
        this.name = name;
        this.children = new ArrayList<>(); //on initialise la liste des enfants
    }

    public void addComponent(Composant component) { // méthode qui ajoute un enfant à la liste
        children.add(component);
    }

    @Override
    public String getName() { //Méthode héritée qui retourne le nom du dossier
        return name;
    }

    public List<Composant> getChildren() {
        return children;
    }

    @Override
    public void ls() { //Méthode héritée qui affiche le contenu du dossier
        System.out.println("Folder: " + name);
        for (Composant component : children) {
            component.ls();
        }
    }
}

