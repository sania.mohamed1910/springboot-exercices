package fr.ensai.demo.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.JoinColumn;

@Entity
@Table(name = "fichiers")
public class Fichier implements Composant{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fichier_id")
    private Integer id;


    @JoinColumn(name = "scan_id")
    private Integer scanId;

    @Column(name = "nom")
    private String nom;

    @Column(name = "date_modification")
    private LocalDateTime dateModification;

    @Column(name = "poids")
    private double poids;

    @Column(name = "type_fichier")
    private String typeFichier;

    @Column(name = "chemin")
    private String chemin;

    // Constructeur vide requis par JPA
    public Fichier() {
    }

    // Constructeur avec arguments pour initialiser l'entité
    public Fichier(String nom, 
                   LocalDateTime date, 
                   double poids, 
                   String typeFichier, 
                   String chemin) {
        this.nom = nom;
        this.dateModification = date;
        this.poids = poids;
        this.typeFichier = typeFichier;
        this.chemin = chemin;
    }

    @Override // Méthode héritée du composant
    public String getName() { //renvoie le nom du fichier ici
        return nom;
    }

    @Override // Méthode héritée du composant
    public void ls() {
        System.out.println("File: " + nom + ", Weight: " + poids + ", Type: " + typeFichier + ", Date: " + dateModification + ", Path: " + chemin);
    }

    // Getters et Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScanId() {
        return scanId;
    }

    public void setScanId(Integer scanId) {
        this.scanId = scanId;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDateTime getDateModification() {
        return dateModification;
    }

    public void setDateModification(LocalDateTime dateModification) {
        this.dateModification = dateModification;
    }

    public Double getPoids() {
        return poids;
    }

    public void setPoids(Double poids) {
        this.poids = poids;
    }

    public String getTypeFichier() {
        return typeFichier;
    }

    public void setTypeFichier(String typeFichier) {
        this.typeFichier = typeFichier;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

}
