package fr.ensai.demo.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.time.LocalDateTime;


@Entity // Cette annotation indique que la classe est une entité JPA
@Table(name = "scans") // Cette annotation spécifie le nom de la table dans la base de données
public class Scan {

    @Id // Indique que ce champ est la clé primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Auto-increment pour la clé primaire
    @Column(name = "scan_id")
    private Integer id;

    @Column(name = "nbr_max_fichiers")
    private Integer nbrMaxFichiers;

    @Column(name = "profondeur_max_arb")
    private Integer profondeurMaxArb;

    @Column(name = "filtre_nom_fichier")
    private String filtreNomFichier;

    @Column(name = "filtre_type_fichier")
    private String filtreTypeFichier;

    @Column(name = "date_scan")
    private LocalDateTime dateScan;

    @Column(name = "temps_exe")
    private Long tempsExecution = 0L; //On initialise à 0 la valeur du temps d'execution, qud l'on mlettra à jour avec le setter
    
    @Column(name = "repertoire")
    private String repertoire; //répertoire du scan

    // Constructeur par défaut
    public Scan() {
        this.tempsExecution = 0L;
    }
    
    // Constructeur avec arguments pour initialiser l'entité
    public Scan(Integer nbrMaxFichiers, 
                Integer profondeurMaxArb, 
                String filtreNomFichier,
                String filtreTypeFichier
                ) {
        
        this.nbrMaxFichiers = nbrMaxFichiers;
        this.profondeurMaxArb = profondeurMaxArb;
        this.filtreNomFichier = filtreNomFichier;
        this.filtreTypeFichier = filtreTypeFichier;
        this.dateScan = LocalDateTime.now();
        // Initialisation de tempsExecution avec 0 par défaut si tempsExecution est null
        this.tempsExecution = (tempsExecution != null) ? tempsExecution : 0L;
    }

    // Getters et setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNbrMaxFichiers() {
        return nbrMaxFichiers;
    }

    public void setNbrMaxFichiers(Integer nbrMaxFichiers) {
        this.nbrMaxFichiers = nbrMaxFichiers;
    }

    public Integer getProfondeurMaxArb() {
        return profondeurMaxArb;
    }

    public void setProfondeurMaxArb(Integer profondeurMaxArb) {
        this.profondeurMaxArb = profondeurMaxArb;
    }

    public String getFiltreNomFichier() {
        return filtreNomFichier;
    }

    public void setFiltreNomFichier(String filtreNomFichier) {
        this.filtreNomFichier = filtreNomFichier;
    }

    public String getFiltreTypeFichier() {
        return filtreTypeFichier;
    }

    public void setFiltreTypeFichier(String filtreTypeFichier) {
        this.filtreTypeFichier = filtreTypeFichier;
    }

    public LocalDateTime getDateScan() {
        return dateScan;
    }

    public void setDateScan(LocalDateTime dateScan) {
        this.dateScan = dateScan;
    }

    public Long getTempsExecution() {
        return tempsExecution;
    }

    public void setTempsExecution(Long tempsExecution) {
        this.tempsExecution = tempsExecution;
    }

    public String getRepertoire() {
        return repertoire;
    }

    public void setRepertoire(String chemin) {
        this.repertoire = chemin;
    }

    
}