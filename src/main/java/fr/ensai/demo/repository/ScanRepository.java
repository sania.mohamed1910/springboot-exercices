package fr.ensai.demo.repository;

import fr.ensai.demo.model.Scan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import java.util.Optional;


@Repository
public interface ScanRepository extends JpaRepository<Scan, Integer> {

    @Query("SELECT AVG(s.tempsExecution) FROM Scan s")
    Double findAverageExecutionTime();

    @SuppressWarnings("null")
    Optional<Scan> findById(Integer id);
    
}