package fr.ensai.demo.repository;

import fr.ensai.demo.model.Fichier;
import jakarta.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository 
public interface FichierRepository extends JpaRepository<Fichier, Integer> {
    Iterable<Fichier> findByNomContainingIgnoreCase(String nom);
    Iterable<Fichier> findByTypeFichierContainingIgnoreCase(String typeFichier);
    Iterable<Fichier> findByScanId(Integer scanId); // Modifiez pour retourner Iterable au lieu de List

    @Transactional
    void deleteByScanId(Integer scanId);

    @Query("SELECT AVG(temps_exe_par_fichier) FROM ( SELECT s.tempsExecution/COUNT(f.id) AS temps_exe_par_fichier FROM Scan s LEFT JOIN Fichier f ON s.id = f.scanId GROUP BY s.id)")
    Double findAverageExecutionTime();
}

