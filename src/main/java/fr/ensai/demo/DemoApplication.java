package fr.ensai.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = "fr.ensai.demo")
public class DemoApplication {

    @Autowired

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}

