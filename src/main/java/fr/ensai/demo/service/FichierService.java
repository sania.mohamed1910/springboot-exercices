package fr.ensai.demo.service;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.repository.FichierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FichierService {

    @Autowired
    private FichierRepository fichierRepository;

    // Ajouter un fichier à un scan
    @SuppressWarnings("null")
    public Fichier addFileToFichier(Fichier fichier) {
        return fichierRepository.save(fichier);
    }

    // Obtenir tous les fichiers d'un scan
    public Iterable<Fichier> getFilesByScanId(Integer scanId) {
        return fichierRepository.findByScanId(scanId);
    }

    // Supprimer un fichier
    @SuppressWarnings("null")
    public void deleteFile(Integer fileId) {
        fichierRepository.deleteById(fileId);
    }

    // Supprimer tous les fichiers d'un scan
    public void deleteFilebyScan(Integer scanId) {
        fichierRepository.deleteByScanId(scanId);
    }

    // Mettre à jour les informations d'un fichier
    @SuppressWarnings("null")
    public Fichier updateFile(Fichier fichier) {
        return fichierRepository.save(fichier);
    }
}
