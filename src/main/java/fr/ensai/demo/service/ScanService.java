package fr.ensai.demo.service;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.FichierRepository; // Import FichierRepository
import fr.ensai.demo.repository.ScanRepository;
import fr.ensai.demo.strategy.LocalStrategy;
import fr.ensai.demo.strategy.S3Strategy;
import fr.ensai.demo.strategy.Strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ScanService {

    @Autowired
    private ScanRepository scanRepository;

    @Autowired
    private FichierRepository fichierRepository;
    
    // Trouver un scan par son id
    public Scan getScan(final Integer id) {
        if (id != null) { // Si on trouve l'id, on retourne le scan sinon une erreur
            Optional<Scan> scanOptional = scanRepository.findById(id);
            if (scanOptional.isPresent()) {
                return scanOptional.get();
            } else {
                throw new RuntimeException("Le scan d'ID n'a pas été trouvé dans la base de données.");
            }
        } else {
            throw new IllegalArgumentException("L'ID ne peut pas être null.");
        }
    }
    
    // Trouver tous les scans
    public Iterable<Scan> getScans() {
        return scanRepository.findAll();
      }

    // Scanner le répertoire
    //@SuppressWarnings("null")
    public Scan createScan(String repertoire, 
                           Integer nbrMaxFichiers, 
                           Integer profondeurMaxArb, 
                           String filtreNomFichier, 
                           String filtreTypeFichier) {
        
        Strategy strategy;
        
        if (repertoire.startsWith("s3://")) {
            // Utilisation de la stratégie S3
            AmazonS3 amazonS3Client = AmazonS3ClientBuilder.defaultClient();
            strategy = new S3Strategy(amazonS3Client, scanRepository, fichierRepository);
        } else {
            // Utilisation de la stratégie locale
            strategy = new LocalStrategy(scanRepository, fichierRepository);
        }
        // Utilisation de la stratégie spécifiée pour scanner le répertoire
        Scan scan = strategy.scanner(repertoire, nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);
    
    return scan;
            
    }


    // Dupliquer un scan en modifiant les attributs
    public Scan duplicateScan(Integer id, 
                            Integer nbrMaxFichiers, 
                            Integer profondeurMaxArb, 
                            String filtreNomFichier, 
                            String filtreTypeFichier) {
        
        // On retrouve le scan dans la bdd
        Scan initialScan = getScan(id);
        String repertoire = initialScan.getRepertoire();

        // On update les attributs selon les valeurs fournies
        if (nbrMaxFichiers == null) {
            nbrMaxFichiers = initialScan.getNbrMaxFichiers();}
    
        if (profondeurMaxArb == null) {
            profondeurMaxArb = initialScan.getProfondeurMaxArb();} 
    
        if (filtreNomFichier == null) {
            filtreNomFichier = initialScan.getFiltreNomFichier();}
        
        if (filtreTypeFichier == null) {
            filtreTypeFichier = initialScan.getFiltreTypeFichier();}

        // On recrée le scan avec les nouveaux paramètres
        return createScan(repertoire, nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);
    }

    // Supprimer un scan
    public void deleteScan(Integer id) {
        if (id != null) {
            scanRepository.deleteById(id);
        } else {
            throw new RuntimeException("Le scan n'est pas dans la base de données.");
        }
    }

    // Rejouer un scan
    public Scan rescan(Integer id) {
        Scan scan = getScan(id);
        if (scan != null){
            // On récupère tous les paramètres
            String repertoire = scan.getRepertoire();
            Integer nbrMaxFichiers = scan.getNbrMaxFichiers();
            Integer profondeurMaxArb = scan.getProfondeurMaxArb();
            String filtreNomFichier = scan.getFiltreNomFichier();
            String filtreTypeFichier = scan.getFiltreTypeFichier();

            // On supprime le scan de base
            scanRepository.delete(scan);

            // puis on le recrée (id différent par contre)
            return createScan(repertoire, nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);
        } else {
            throw new RuntimeException("Le scan n'est pas dans la base de données.");
        }
    }

    // Comparer deux scans
    public String compareScans(int id1, int id2) {
        // Obtenez les scans correspondants à id1 et id2 en utilisant votre service
        Scan scan1 = getScan(id1);
        Scan scan2 = getScan(id2);
    
        // Comparaison des attributs des scans
        StringBuilder result = new StringBuilder();
        result.append("Comparaison entre scan ").append(id1).append(" et scan ").append(id2).append(" :\n");
    
        
        result.append("Nombre maximal de fichiers : ").append(scan1.getNbrMaxFichiers()).append(" vs ").append(scan2.getNbrMaxFichiers()).append("\n");
        result.append("Profondeur maximale de l'arbre : ").append(scan1.getProfondeurMaxArb()).append(" vs ").append(scan2.getProfondeurMaxArb()).append("\n");
        result.append("Filtre nom de fichier : ").append(scan1.getFiltreNomFichier()).append(" vs ").append(scan2.getFiltreNomFichier()).append("\n");
        result.append("Filtre type de fichier : ").append(scan1.getFiltreTypeFichier()).append(" vs ").append(scan2.getFiltreTypeFichier()).append("\n");
        result.append("Date de scan : ").append(scan1.getDateScan()).append(" vs ").append(scan2.getDateScan()).append("\n");
        result.append("Temps d'exécution : ").append(scan1.getTempsExecution()).append(" vs ").append(scan2.getTempsExecution()).append("\n");
        
        Integer[] identifiants = {id1, id2}; //On veut affichier les fichiers de chaque scan
        for (Integer id : identifiants ) {

            Iterable<Fichier> fichiers = fichierRepository.findByScanId(id); //liste des fichiers du scan id
            List<String> liste_noms_fichiers = new ArrayList<>();             //liste des noms des fichiers
            
            for (Fichier fichier : fichiers) {
                String nomfichier = fichier.getName(); //on récup le nom
                liste_noms_fichiers.add(nomfichier);
            }

            result.append("Liste des fichiers du scan ").append(id).append(" : ").append(liste_noms_fichiers).append("\n");
        }
        return result.toString();
    }
    
    // Obtenir les statistiques des scans 
    public String statistiques() {
        Double moyenneParRepertoire = scanRepository.findAverageExecutionTime();
        Double moyenneParFichier = fichierRepository.findAverageExecutionTime();

        return "Temps d'exécution moyen par répertoire : "+ moyenneParRepertoire.toString() + " ms, \nTemps d'exécution moyen par fichier : "+ moyenneParFichier.toString()+" ms.";
    }

}

  