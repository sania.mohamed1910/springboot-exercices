package fr.ensai.demo.strategy;

import java.io.File; // File de java.io est utilisée pour manipuler les fichiers et les répertoires du système de fichiers local.
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import fr.ensai.demo.model.Composant;
import fr.ensai.demo.model.Dossier;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.FichierRepository;
import fr.ensai.demo.repository.ScanRepository;


public class LocalStrategy implements Strategy {
    private final ScanRepository scanRepository;
    private final FichierRepository fichierRepository;

    public LocalStrategy(ScanRepository scanRepository, FichierRepository fichierRepository) {
        this.scanRepository = scanRepository;
        this.fichierRepository = fichierRepository;
    }

    @Override //méthode héritée qui permet de scanner le répertoire
    public Scan scanner(String chemin, Integer nbrMaxFichiers, Integer profondeurMaxArb, String filtreNomFichier, String filtreTypeFichier) {
        Long debut = System.currentTimeMillis();  // début du scan

        // Récupération des fichiers du répertoire
        // Converion en Composant
        File repertoire = new File(chemin);
        List<Composant> listeComposants = listerComposants(repertoire); // On convertit tous les objets du répertoire en composants
        listerFichiers(listeComposants, nbrMaxFichiers,profondeurMaxArb, filtreNomFichier, filtreTypeFichier); //on save tous les fichiers selon les filtres
        
        // Enregistrement du scan dans la table Scans
        Long fin = System.currentTimeMillis();
        Long tempsExecution = fin - debut;
        LocalDateTime dateScan = LocalDateTime.now();

        // Création et sauvegarde du scan
        Scan scan = new Scan();
        scan.setRepertoire(chemin);
        scan.setNbrMaxFichiers(nbrMaxFichiers);
        scan.setProfondeurMaxArb(profondeurMaxArb);
        scan.setFiltreNomFichier(filtreNomFichier); 
        scan.setFiltreTypeFichier(filtreTypeFichier);
        scan.setDateScan(dateScan);
        scan.setTempsExecution(tempsExecution); 
        
        scanRepository.save(scan);

        return scan;
    }

    @Override
    public Boolean filtrer(String filtre, Fichier fichier){ //fichier est filtré ou non
        String nomFichier = fichier.getName();
        if (filtre==null){
            return true;
        }
        return nomFichier.contains(filtre);
    }

    @Override
    public  void listerFichiers(List<Composant> composants, Integer nbrMaxFichiers, Integer profondeurMaxArb, String filtreNomFichier, String filtreTypeFichier) {
        Integer nbrFichiers = 0;
        Integer profondeurArbo = 0;
        for (Composant composant : composants){
            if (composant instanceof Fichier){ // Si le compo est un fichier
                Fichier fichier = (Fichier) composant;
                Boolean filtreNom = filtrer(filtreNomFichier, fichier);
                Boolean filtreType = filtrer(filtreTypeFichier, fichier);
                if (filtreNom && filtreType && nbrFichiers<=nbrMaxFichiers){ //si les fichiers passent les filtres et qu'on n'a pas atteint le max de fichiers
                    nbrFichiers +=1; //on l'ajoute à la bdd
                    fichierRepository.save(fichier);
                }
            }
            else { //si c'est un dossier
                if (profondeurArbo<= profondeurMaxArb){
                    profondeurArbo +=1;
                    Dossier dossier = (Dossier) composant;
                    listerFichiers(dossier.getChildren(), nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);
                    }
                }
        }           
    }

    public List<Composant> listerComposants(File repertoire) { //Cnvertit tous les objets du repertoire en Fichier et Dossier
        List<Composant> composants = new ArrayList<>();

        File[] fichiers = repertoire.listFiles();
        if (fichiers != null) {
            for (File fichier : fichiers) {
                if (fichier.isDirectory()) { //si le composant est un dossier
                    Dossier sousDossier = new Dossier(fichier.getName());
                    for (Composant children : sousDossier.getChildren()) {
                        sousDossier.addComponent(children);
        }
                
                    composants.add(sousDossier);
                } else {
                    String nom = fichier.getName();
                    long datemilis = fichier.lastModified(); 
                    LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(datemilis), ZoneId.systemDefault());
                    long poids = fichier.length();
                    String typeFichier = extension(nom); 
                    String chemin = fichier.getAbsolutePath();
                    
                    Fichier fichierObjet = new Fichier(nom, date, poids, typeFichier, chemin);
                    composants.add(fichierObjet);
                }
            }
        }
        return composants;
    }
                
    private String extension(String nomFichier) {
        int dernierPointIndex = nomFichier.lastIndexOf('.');
        if (dernierPointIndex == -1 || dernierPointIndex == nomFichier.length() - 1) {
            // Aucune extension trouvée
            return "";
        }
        return nomFichier.substring(dernierPointIndex + 1);
    }
    
    
        
        
}

