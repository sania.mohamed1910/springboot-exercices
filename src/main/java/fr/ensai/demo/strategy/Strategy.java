package fr.ensai.demo.strategy;

import java.util.List;

import fr.ensai.demo.model.Composant;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;

public interface Strategy {
    void listerFichiers(List<Composant> composants, Integer profondeurMaxArb, Integer nbrMaxFichiers, String filtreNomFichier, String filtreTypeFichier);

    Scan scanner(String chemin, Integer nbrMaxFichiers, Integer profondeurMaxArb, String filtreNomFichier, String filtreTypeFichier);

    Boolean filtrer(String filtre, Fichier fichier);
}
