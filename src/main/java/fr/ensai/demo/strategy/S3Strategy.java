package fr.ensai.demo.strategy;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import fr.ensai.demo.model.Composant;
import fr.ensai.demo.model.Dossier;
import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.repository.FichierRepository;
import fr.ensai.demo.repository.ScanRepository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class S3Strategy implements Strategy {
    private AmazonS3 s3Client;
    private ScanRepository scanRepository;
    private FichierRepository fichierRepository;

    public S3Strategy(AmazonS3 s3Client, ScanRepository scanRepository, FichierRepository fichierRepository) {
        this.s3Client = s3Client;
        this.scanRepository = scanRepository;
        this.fichierRepository = fichierRepository;
    }

    @Override
    public Scan scanner(String bucketName, Integer nbrMaxFichiers, Integer profondeurMaxArb, String filtreNomFichier, String filtreTypeFichier) {
        Long debut = System.currentTimeMillis();  // début du scan

        // Récupération des objets du bucket S3
        List<Composant> listeComposants = listerComposants(bucketName);

        // Enregistrement des fichiers trouvés dans la table Fichiers
        listerFichiers(listeComposants, nbrMaxFichiers,profondeurMaxArb, filtreNomFichier, filtreTypeFichier); //on save tous les fichiers selon les filtres

        // Enregistrement du scan dans la table Scans
        Long fin = System.currentTimeMillis();
        Long tempsExecution = fin - debut;
        LocalDateTime dateScan = LocalDateTime.now();

        // Création et sauvegarde du scan
        Scan scan = new Scan();
        String repertoire = "s3://" + bucketName;
        scan.setRepertoire(repertoire);
        scan.setNbrMaxFichiers(nbrMaxFichiers);
        scan.setProfondeurMaxArb(profondeurMaxArb);
        scan.setFiltreNomFichier(filtreNomFichier);
        scan.setFiltreTypeFichier(filtreTypeFichier);
        scan.setDateScan(dateScan);
        scan.setTempsExecution(tempsExecution);

        scanRepository.save(scan);
        
        return scan;
    }

    public List<Composant> listerComposants(String bucketName) { //Cnvertit tous les objets du repertoire en Fichier et Dossier
    
        List<Composant> composants = new ArrayList<>();
        List<S3ObjectSummary> objectSummaries = s3Client.listObjectsV2(bucketName).getObjectSummaries();

        for (S3ObjectSummary objectSummary : objectSummaries) {
            String key = objectSummary.getKey();
            S3Object object = s3Client.getObject(bucketName, key);

            if (object.getKey().endsWith("/")) { // Si le composant est un dossier
                Dossier dossier = new Dossier(key.substring(0, key.length() - 1)); // Supprimer le '/' à la fin du nom du dossier
                composants.add(dossier);
            } else { // Si c'est un fichier
                String nom = key.substring(key.lastIndexOf("/") + 1); // Récupérer le nom du fichier
                long datemilis = objectSummary.getLastModified().getTime();
                LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(datemilis), ZoneId.systemDefault());
                long poids = objectSummary.getSize();
                String typeFichier = extension(nom);
                String chemin = nom;
                Fichier fichier = new Fichier(nom, date, poids, typeFichier, chemin);
                composants.add(fichier);
            }
        }
     
        return composants;
    }
    
    private String extension(String nomFichier) {
        int dernierPointIndex = nomFichier.lastIndexOf('.');
        if (dernierPointIndex == -1 || dernierPointIndex == nomFichier.length() - 1) {
            // Aucune extension trouvée
            return "";
        }
        return nomFichier.substring(dernierPointIndex + 1);
    }

    @Override
    public  void listerFichiers(List<Composant> composants, Integer nbrMaxFichiers, Integer profondeurMaxArb, String filtreNomFichier, String filtreTypeFichier) {
        Integer nbrFichiers = 0;
        Integer profondeurArbo = 0;
        for (Composant composant : composants){
            if (composant instanceof Fichier){ // Si le compo est un fichier
                Fichier fichier = (Fichier) composant;
                Boolean filtreNom = filtrer(filtreNomFichier, fichier);
                Boolean filtreType = filtrer(filtreTypeFichier, fichier);
                if (filtreNom && filtreType && nbrFichiers<=nbrMaxFichiers){ //si les fichiers passent les filtres et qu'on n'a pas atteint le max de fichiers
                    nbrFichiers +=1; //on l'ajoute à la bdd
                    fichierRepository.save(fichier);
                }
            }
            else { //si c'est un dossier
                if (profondeurArbo<= profondeurMaxArb){
                    profondeurArbo +=1;
                    Dossier dossier = (Dossier) composant;
                    listerFichiers(dossier.getChildren(), nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);
                    }
                }
        }           
    }

    @Override
    public Boolean filtrer(String filtre, Fichier fichier) {
        String nomFichier = fichier.getName();
        if (filtre==null){
            return true;
        }
        return nomFichier.contains(filtre);
    }
}


