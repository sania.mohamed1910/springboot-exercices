package fr.ensai.demo.controller;

import fr.ensai.demo.model.Fichier;
import fr.ensai.demo.model.Scan;
import fr.ensai.demo.service.FichierService;
import fr.ensai.demo.service.ScanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/scans") // Ce contrôleur gère les routes sous '/scans'
public class ScanController {

    @Autowired
    private ScanService scanService;

    @Autowired
    private FichierService fichierService;

    // Créer un nouveau scan
    //@PostMapping("/json")
    //public ResponseEntity<Scan> createScanBody(@RequestBody Scan scan) {
    //    Scan newScan = scanService.createScan(scan);
    //    return new ResponseEntity<>(newScan, HttpStatus.CREATED);
    //}

    // Créer un nouveau scan
    @PostMapping
    public ResponseEntity<Scan> createScan(@RequestParam("repertoire") String repertoire,
                                           @RequestParam("profondeurMaxArb") int profondeurMaxArb,
                                           @RequestParam("nbrMaxFichiers") int nbrMaxFichiers,
                                           @RequestParam(value = "filtreNomFichier", required = false) String filtreNomFichier,
                                           @RequestParam(value = "filtreTypeFichier", required = false) String filtreTypeFichier) {
        
        Scan scan = scanService.createScan(repertoire,profondeurMaxArb, nbrMaxFichiers, filtreNomFichier, filtreTypeFichier);
        return new ResponseEntity<>(scan, HttpStatus.CREATED);
    }

    // Dupliquer un scan existant
    @PostMapping("/duplicate/{id}")
    public ResponseEntity<Scan> duplicateScan(@PathVariable Integer id,
                                               @RequestParam(value = "nbrMaxFichiers", required = false) Integer nbrMaxFichiers,
                                               @RequestParam(value = "profondeurMaxArb", required = false) Integer profondeurMaxArb,
                                               @RequestParam(value = "filtreNomFichier", required = false) String filtreNomFichier,
                                               @RequestParam(value = "filtreTypeFichier", required = false) String filtreTypeFichier) {
        // Appel de la méthode duplicateScan du service
        Scan duplicatedScan = scanService.duplicateScan(id, nbrMaxFichiers, profondeurMaxArb, filtreNomFichier, filtreTypeFichier);
        
        return new ResponseEntity<>(duplicatedScan, HttpStatus.CREATED);
       
    }

    // Supprimer un scan
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteScan(@PathVariable Integer id) {
        scanService.deleteScan(id);
        fichierService.deleteFilebyScan(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // Rejouer un scan
    @PutMapping("/rescan/{id}")
    public ResponseEntity<Scan> rescan(@PathVariable Integer id) {
        Scan rescanResult = scanService.rescan(id);
        if (rescanResult != null) {
            return new ResponseEntity<>(rescanResult, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // Comparer deux scans
    @GetMapping("/compare/{id1}vs{id2}")
    public ResponseEntity<String> compareScans(@PathVariable Integer id1, @PathVariable Integer id2) {
        String resultat = scanService.compareScans(id1, id2);
        return ResponseEntity.ok(resultat);
    }

    // Lister tous les scans 
    @GetMapping("/all")
    public ResponseEntity<List<Scan>> getScans() {
        List<Scan> scans = (List<Scan>) scanService.getScans();
        return ResponseEntity.ok(scans);
    }

    // Lister les fichiers d'un scan
    @GetMapping("/fichiers/{scanId}")
    public ResponseEntity<List<Fichier>> getFilesByScanId(@PathVariable Integer scanId){
        List<Fichier> fichiers = (List<Fichier>) fichierService.getFilesByScanId(scanId);
        return ResponseEntity.ok(fichiers);
    }

    // Calculer les statistiques
    @GetMapping("/statistiques")
    public ResponseEntity<String> statistiques() {
        String stats = scanService.statistiques();
        return ResponseEntity.ok(stats);
    }
}
